import React ,{ useState , useEffect }from 'react'
import { MdDeleteOutline } from "react-icons/md";
import { FaRegStar } from "react-icons/fa";
import Modal from './Modal';
const OdersItem = ({item , deleteOder , approveDeleteOders , approveDeleteOdFunc , starListAdd , deleteStarList}) => {
    const [deleteModal , setDeleteModal] = useState(false)
    const [addStar, setaddStar] = useState(false)
    
    
    const isDeleteHand = ()=>{
        setDeleteModal(!deleteModal)
    }
    const change = () => {
        setaddStar(!addStar)
    
    }
    useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('star'));
        if (starData) {
          starData.forEach((element) => {
            if (element.id === item.id) {
              change();
            }
          });
        }
      }, []);


  return (
    <div>

{
             
                
                    <div className="buy-item">
                         <FaRegStar className={`star_shop ${(addStar) && `star-active`}`} onClick={() => {
           change();
           !addStar ?  starListAdd(item) : deleteStarList(item)
        }} />
                        <img src={"images/" + item.img} alt="oders"/>
                    <h2 >{item.name}</h2>
                    <p>color: {item.color}</p>
                        <b>price : {item.price} $</b>
                        <MdDeleteOutline className='delete' onClick={()=> {
                            approveDeleteOdFunc(item)
                            isDeleteHand()
                        }}/>
                    </div> 
          
          }
              {deleteModal && <Modal Modal_text="Do you want delete this item from buy list?"
        titleName="Delete"
        isDeleteHand={isDeleteHand}
        deleteOder={deleteOder}
        approveDeleteOders={approveDeleteOders}
        isModalFor={"Delete"} 
         
           />}

    </div>
  )
}

export default OdersItem