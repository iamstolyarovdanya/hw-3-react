import React, { useState } from 'react'

const Button = ({chosee, mainOder  , modalToogle , isBtnFor , mainChoose , itemCard , stanBtn ,  isDeleteHand , approveDeleteOders , deleteOder , changeStanBtn}) => {
 
  return (
    <>
    {
        isBtnFor === `addToBuy` &&  <button  disabled={stanBtn}      className={` ${(stanBtn) && `btn-unactive`}`}
        onClick={() => {     
               
                modalToogle();
                mainChoose(itemCard)
               
       }}>Добавить в корзину</button>
    }
     {
        isBtnFor === `addToBuyList` &&   <button onClick={() => {
                           
            chosee(...mainOder)
            modalToogle();
            changeStanBtn()      
        }} >OK</button>
    }
  
     {
        isBtnFor === `Delete_Oder` &&   <button onClick={() => {
                           
         
          modalToogle();
          deleteOder(approveDeleteOders)
    
        }} >OK</button>
    }
    </>
  )
}

export default Button