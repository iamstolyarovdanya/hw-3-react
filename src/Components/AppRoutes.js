import React, {useState} from 'react'
import { Route , Routes  } from 'react-router-dom';
import {Oders} from '../Pages/Oders';
import {Star} from '../Pages/Star';
import {Card} from "../Pages/Card";
import {Header} from "./Header";


const AppRoutes = ({oders ,
     starList , 
     deleteOder,
      deleteStarList , 
      item , 
      Choose , 
      mainChoose , 
      mainoders, 
      starListAdd,
    }) => {
      const [approveDeleteOders  , setApproveDeleteOders] = useState()
      const approveDeleteOdFunc = (item)=>{
        setApproveDeleteOders(item)
    }
  return (
    <>
      <Routes>
            <Route path='/' element={<Header  oders={oders}
                 starList={starList} 
                 deleteOder={deleteOder}
                 deleteStarList={deleteStarList} />}>
                <Route index path='/' element={
                <Card Item={item}
                    Choose={Choose}
                    mainChoose={mainChoose}
                    mainoders={mainoders}
                    starListAdd={starListAdd}
                    starList={starList}
                    deleteStarList={deleteStarList}
                    oders={oders}
              
                 
                    />}/>
                <Route path='/Shop' element={ <Oders 
                item={oders} 
                deleteOder={deleteOder}
                approveDeleteOders={approveDeleteOders}
                approveDeleteOdFunc={approveDeleteOdFunc}
                starListAdd={starListAdd}
                deleteStarList={deleteStarList}
                />   }/>
                <Route path='/Star' element={ <Star item={starList} deleteStarList={deleteStarList} />}/>
            </Route>
           </Routes>
    
    
    
    </>
  )
}

export default AppRoutes