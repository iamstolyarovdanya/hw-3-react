import React from 'react';
import PropTypes from 'prop-types';

import ButtonCloseModal from './Buttons/ButtonCloseModal';
import ButtonDeleteItem from './Buttons/ButtonDeleteItem';
import ButtonAddToList from './Buttons/ButtonAddToList';
function Modal({ Modal_text, titleName, modalToogle, chosee, mainOder,  changeStanBtn , changeStanBtnApp , isModalFor,isDeleteHand ,deleteOder  ,  approveDeleteOders}) {
    return (
        <div>
            <div id="blur" className="blur" onClick={(e) => (
                e.target.id === "blur" && (isModalFor === "Approve" ? modalToogle() : isDeleteHand() )
            )}>

                <div className="modal">
                    <h2 className="modal__title">{titleName}</h2>
                    <p>{Modal_text}</p>

                   { isModalFor === "Approve" && <div className="modal__btn-wrapper">
                        <ButtonAddToList 
                        chosee={chosee} mainOder={mainOder} modalToogle={modalToogle} isBtnFor="addToBuyList" 
                         changeStanBtn={ changeStanBtn}/>
                       <ButtonCloseModal chosee={chosee} mainOder={mainOder} modalToogle={modalToogle} isBtnFor="closeModal"/>
                    </div>
                     }
                        { isModalFor === "Delete" && <div className="modal__btn-wrapper">
                        <ButtonDeleteItem
                        deleteOder={deleteOder} approveDeleteOders={approveDeleteOders} modalToogle={isDeleteHand} isBtnFor="Delete_Oder" 
                        />
                       <ButtonCloseModal chosee={chosee} mainOder={mainOder} modalToogle={isDeleteHand} isBtnFor="closeModalDelete"/>
                    </div>
                     }
                </div>
            </div>
        </div>
    );
}
Modal.propTypes = {
    Modal_text: PropTypes.string,
    titleName: PropTypes.string,
  
    modalToogle: PropTypes.func,
}
export default Modal;