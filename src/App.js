import './App.css';
import React, { useEffect, useState } from "react";
import AppRoutes from './Components/AppRoutes';

function App() {
    const [mainoders, SetMainOders] = useState([])
    const [oders, setOders] = useState([])
    const [item, setItem] = useState()
    const [starList, setStarList] = useState([])
    useEffect(() => {
        setOders(JSON.parse(localStorage.getItem(`oders`)) || [] )
    }, [])
    useEffect(() => {
        localStorage.setItem(`oders`, JSON.stringify(oders))
    }, [oders])
    useEffect(() => {
        setStarList(JSON.parse(localStorage.getItem(`star`)) || []  )
    }, [])
    useEffect(() => {
        localStorage.setItem(`star`, JSON.stringify(starList))
    }, [starList])

    
    const deleteOder = (item) => {
        setOders([...oders].filter((el) => el.id !== item.id))
    }
    const deleteStarList = (item) => {
        setStarList([...starList].filter((el) => el.id !== item.id))
    }
    const starListAdd = (item) => {
        let add = false;
        starList.forEach((el) => {
            if (el.id === item.id) {
                add = true
            }
        })
        !add &&
            setStarList([...starList, item]);
    }
    const mainChoose = (item) => {
        SetMainOders([item]);
    }

    const Choose = (item) => {
        let add = false;
        oders.forEach((el) => {
            if (el.id === item.id) {
                add = true
            }
        })
        !add && setOders([...oders, item]);
    }

    useEffect(() => {
        const fetchItems = async () => {
            try {
                const response = await fetch(`/getItem.json`);
                const data = await response.json()
                setItem(data.item)
            } catch (er) {
                console.warn(er)
            }
        };
        fetchItems();
    }, [])
    return (<>
        {item && <div className="App">
            <AppRoutes
             oders={oders} 
             starList={starList}
             deleteOder={deleteOder}
             deleteStarList={deleteStarList}
             item={item}
             Choose={Choose}
             mainChoose={mainChoose}
             mainoders={mainoders}
             starListAdd={starListAdd}
         
             />
        </div>}
    </>
    );
}


export default App;
