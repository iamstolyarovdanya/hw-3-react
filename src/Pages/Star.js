import React from 'react';
import PropTypes from 'prop-types';
import { FaRegStar } from "react-icons/fa";
function Star({item , deleteStarList}) {
    return (
        <div className='Oders-wrapper'>
            {
                item.length > 0 ?
                item.map((el)=>( 
                    <div className="buy-item" key={el.id}>
                        <FaRegStar className='Star-delete' style={{color: 'orange'}} onClick={()=>{
                            deleteStarList(el)
                        }} />
                        <img src={"images/" + el.img} alt="oders"/>
                        <h2>{el.name}</h2>
                        <p>color: {el.color}</p>
                        <b>{el.price} $</b>
                    </div>
                )) : <div className='Nothing'> Did you add something? No.</div>
            }

        </div>
    );
}
Star.propTypes = {
    item : PropTypes.array.isRequired,
  
    }
export {Star};