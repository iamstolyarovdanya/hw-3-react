import PropTypes from 'prop-types';
import OdersItem from '../Components/OdersItem';
function Oders({item ,deleteOder , approveDeleteOders  , approveDeleteOdFunc , starListAdd, deleteStarList}) {

    return (
        <div className='Oders-wrapper'>
  { item.length > 0 ?
                item.map((el)=>(
                    <OdersItem 
                    key={el.id} 
                    item = {el}
                    deleteOder={deleteOder}
                    approveDeleteOders={approveDeleteOders}
                    approveDeleteOdFunc={approveDeleteOdFunc}
                    starListAdd={starListAdd}
                     deleteStarList={deleteStarList}
                    />
                  )): <div className='Nothing'> Did you chose something? No.</div>}
        </div>
    );
}
Oders.propTypes = {
    item : PropTypes.array.isRequired,
  
    }

export {Oders};